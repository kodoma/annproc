package com.kodoma.visitor;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.dom.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Обработчик класса.
 * Created on 20.06.2018.
 * @author Kodoma.
 */
public class ClassVisitor {

    public void visit(final String source) {
        final ASTParser parser = ASTParser.newParser(AST.JLS8);
        parser.setSource(source.toCharArray());
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        final CompilationUnit unit = (CompilationUnit)parser.createAST(new NullProgressMonitor());

        unit.accept(new ASTVisitor() {
            private final List<MethodDeclaration> methods = new ArrayList<>();

            public List<MethodDeclaration> perform(ASTNode node) {
                node.accept(this);
                return this.getMethods();
            }

            @Override
            public boolean visit (final MethodDeclaration method) {
                methods.add(method);
                return super.visit(method);
            }

            /**
             * @return an immutable list view of the methods discovered by this visitor
             */
            public List <MethodDeclaration> getMethods() {
                return Collections.unmodifiableList(methods);
            }
        });
    }
}
