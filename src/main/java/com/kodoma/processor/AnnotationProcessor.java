package com.kodoma.processor;

import com.kodoma.visitor.ClassVisitor;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class AnnotationProcessor extends AbstractProcessor {

    final static ClassVisitor visitor = new ClassVisitor();

    @Override
    public final boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        /*for (TypeElement elem : ElementFilter.typesIn(roundEnv.getRootElements())) {
            final String className = processingEnv.getElementUtils().getBinaryName(elem).toString()
                                                  .replace(".", "\\").concat(".java");
            final File file = getFileByClass(className);

            if (file != null && file.exists()) {
                visitor.visit(readFile(file));
            }
        }*/
        return false;
    }

    private File getFileByClass(final String className) {
        try {
            final File dirs = new File(".");
            final String dirPath = dirs.getCanonicalPath() + File.separator + "src" + File.separator;
            final File root = new File(dirPath);
            final File[] files = root.listFiles();

            for (File f : files) {
                return new File(f.getPath() + "\\java\\" + className);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String readFile(final File file) {
        final StringBuilder sb = new StringBuilder();
        final String ls = System.getProperty("line.separator");
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(file.getCanonicalPath()))) {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append(ls);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
